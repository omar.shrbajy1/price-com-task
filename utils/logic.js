const {readFile} = require('./helpers.js');

/**
 *
 * it takes from as date and to as a date, iterate over them and generate all days between them
 *
 * @param from
 * @param to
 * @returns {Array}
 */
const getRangeDays = (from, to) => {
    from = new Date(from);
    to = new Date(to);
    const dates = [];
    while (from <= to) {
        dates.push(from.toISOString().slice(0, 10))
        from.setDate(from.getDate() + 1);
    }
    return dates;
}


/**
 *
 * @param day
 * @param carrier
 * @param fromCountry
 * @param toCountry
 * @returns {Promise<Array>}
 */

const getPossibilityDayPerCarrier = async (day, carrier, fromCountry, toCountry) => {
    try {
        //read json files
        const carriers = JSON.parse(await readFile('data/carriers.json'));
        const countries = JSON.parse(await readFile('data/countries.json'));
        // find the carrier that being passed in function parameter
        carrierObj = carriers.find(item => item.id === carrier);

        // if carrier object does not contain fromCountry and toCountry return `nothing`
        if (!carrierObj.countries.includes(fromCountry) && !carrierObj.countries.includes(toCountry))
            return [];
        const allPossibilities = [];

        // Find the countries inside countries Json
        const fromCountryObj = countries.find(item => item.id === fromCountry)
        const toCountryObj = countries.find(item => item.id === toCountry)

        // if the fromCountry is a holiday at that day return nothing
        if (fromCountryObj.holidays.includes(day))
            return [];

        // 2 days if fromCountry and toCountry are a different countries otherwise one day (be is a special case)
        const deliveryTime = fromCountry === toCountry || fromCountry === 'be' || toCountry === 'be' ? 1 : 2;
        let end = new Date(new Date(day).setDate(new Date(day).getDate() + deliveryTime)).toISOString().slice(0, 10);

        // Keep increasing days until there are no holidays
        while (toCountryObj.holidays.includes(end))
            end = new Date(new Date(end).setDate(new Date(end).getDate() + 1)).toISOString().slice(0, 10);


        allPossibilities.push({
            from: fromCountry,
            to: toCountry,
            submissionDate: day,
            deliveryDate: end,
            carrier: carrier,
            price: carrierObj.price
        });

        return allPossibilities;
    } catch (e) {
        console.log("EXCE", e)
    }
}


const getShippingPossibilitiesForRangeOfDays = async (from, to, fromCountry, toCountry) => {

    // read carrier json file
    const carriers = JSON.parse(await readFile('data/carriers.json'));
    let allPossibilities = [];

    const days = getRangeDays(from, to);
    for (let carrier of carriers) {
        for (let day of days) {
            // call  getPossibilityDayPerCarrier for  each day while iterating over them
            allPossibilities = [...await getPossibilityDayPerCarrier(day, carrier.id, fromCountry, toCountry), ...allPossibilities];
        }
    }
    return allPossibilities;
}


const getSupplierPerShippingPossibility = async (shippingPossibility) => {
    const suppliers = JSON.parse(await readFile('data/suppliers.json'));
    return suppliers.reduce((acc, item) => {

        // if one of those conditions are true, then there are no suppliers (empty array)
        if ((item.holidays && item.holidays.includes(shippingPossibility.submissionDate)) ||
            (item.address.country !== shippingPossibility.fromCountry) ||
            (!item.carriers.includes(shippingPossibility.carrier))
        )
            return [...acc];
        // otherwise add supplier item to the array
        return [...acc, item]
    }, [])
}

module.exports = {
    getShippingPossibilitiesForRangeOfDays,
    getPossibilityDayPerCarrier,
    getSupplierPerShippingPossibility
}