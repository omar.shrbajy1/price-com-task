/**
 *  Those are the rules which I am validating the request payload when requesting an endpoint
 */

exports.getPossibilityDayPerCarrierRules = {
    day: 'required|date',
    carrier: 'required|string',
    fromCountry: 'required|string',
    toCountry: 'required|string',
};

exports.getShippingPossibilitiesForRangeOfDaysRules = {
    from: 'required|date',
    to:'required|date',
    fromCountry:'required|string',
    toCountry:'required|string',
}

exports.getSupplierPerShippingPossibilityRules = {
    fromCountry: 'required|string',
    toCountry:'required|string',
    submissionDate:'required|date',
    deliveryDate:'required|date',
    carrier:'required|string',
}