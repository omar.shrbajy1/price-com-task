const fs = require('fs');

/**
 * I call it inside each point to return the body and set the status code when the client request a certain endppint
 *
 * @param status
 * @param body
 * @returns {{statusCode: number, body: string}}
 */


module.exports.generateStatus = (status, body) => {

    return {
        statusCode: status,
        body: JSON.stringify(
            {
                ...body
            },
            null,
            2
        ),
    };

};


/**
 *
 * For reading json files and return it as a promise, which makes it more readable
 *
 * @param path
 * @returns {Promise<any>}
 */

module.exports.readFile = (path) => {

    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if(err)
               return reject(err)
            return resolve(data);
        })
    })


}

