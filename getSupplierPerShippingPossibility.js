'use strict';

let Validator = require('validatorjs');
const {generateStatus} = require('./utils/helpers');
const {getSupplierPerShippingPossibility} = require('./utils/logic');
const {getSupplierPerShippingPossibilityRules} = require('./utils/rules');


module.exports.handler = async event => {
    let validation = new Validator(event.queryStringParameters, getSupplierPerShippingPossibilityRules);
    if (validation.fails())
        return generateStatus(400, {message: validation.errors});
    const {submissionDate, deliveryDate, fromCountry, toCountry, carrier} = event.queryStringParameters;
    return generateStatus(200, {message: await getSupplierPerShippingPossibility({submissionDate, deliveryDate, fromCountry, toCountry, carrier})})
};

