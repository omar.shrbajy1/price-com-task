'use strict';

let Validator = require('validatorjs');
const {generateStatus} = require('./utils/helpers');
const {getShippingPossibilitiesForRangeOfDays} = require('./utils/logic');
const {getShippingPossibilitiesForRangeOfDaysRules} = require('./utils/rules');


module.exports.handler = async event => {
    let validation = new Validator(event.queryStringParameters, getShippingPossibilitiesForRangeOfDaysRules);
    if (validation.fails())
        return generateStatus(400, {message: validation.errors});
    const {from, to, fromCountry, toCountry} = event.queryStringParameters;
    return generateStatus(200, {message: await getShippingPossibilitiesForRangeOfDays(from, to, fromCountry, toCountry)})
};

