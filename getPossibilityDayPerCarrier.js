'use strict';

const fs = require('fs');
const {generateStatus} = require('./utils/helpers');
const {getPossibilityDayPerCarrier} = require('./utils/logic');
const {getPossibilityDayPerCarrierRules} = require('./utils/rules');

let Validator = require('validatorjs');

module.exports.handler = async event => {
    let validation = new Validator(event.queryStringParameters, getPossibilityDayPerCarrierRules);
    if (validation.fails())
        return generateStatus(400, {message: validation.errors});
    const {day, carrier, fromCountry, toCountry} = event.queryStringParameters;
    return generateStatus(200, {message: await getPossibilityDayPerCarrier(day, carrier, fromCountry, toCountry)})
};

