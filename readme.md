# Print.com task
I had some troubles while working on this task, because I was not sure of my understanding so I hope that I solved it correctly and I  hope that you like my solution


## Install project

1- first of all, you need to do  `npm install`

2- then you can do `sls deploy` to deploy to your AWS account


##  How I  solved the task

I tried to add comments as much as I can  so  it can be clear for you, however I will write a brief explanation of what I have done.

**First question** :  Return shipping possibilities for a range of days

The file that solve this question is `getShippingPossibilitiesForRangeOfDays.js`

Input is :  (`from`, `to` ,`fromCountry`, `toCountry`)
please note that you should send this data as a query parameters, example:
```
https://AWS_URL?from=2021-05-01&to=2021-05-28&fromCountry=nl&toCountry=fr
```

### How I solved it:

- first of all it generates the days that are between `from` and `to`
- it iterates over carrier and over the generated days to generate an array of objects, each object contains submission and delivery dates based on the assumptions you have added in the task.

- Example object
```
  {
            "from": "nl",
            "to": "fr",
            "submissionDate": "2021-05-02",
            "deliveryDate": "2021-05-04",
            "carrier": "PNL",
            "price": 5
   }
```

**Second question** :  Return the supplier per shipping possibilities

The file that solve this question is `getSupplierPerShippingPossibility.js`

Input is :  {`submissionDate`, `deliveryDate`, `fromCountry`, `toCountry`, `carrier`}
please note that you should send this data as a query parameters, example:
```
https://AWS_URL?fromCountry=nl&toCountry=fr&submissionDate=2021-05-02&deliveryDate=2021-05-04&carrier=PNL&price=5
```

### How I solved it:

- For this endpoint you need to enter an object of a shipping possibility and it will search for suppliers that doesn't have a holiday at the submissionDate, have the same carrier and has a country same as `fromCountry`  value  

- Example object
```
    {
            "id": "TMB",
            "address": {
                "country": "nl"
            },
            "holidays": [
                "2021-05-07",
                "2021-05-08"
            ],
            "carriers": [
                "PNL",
                "DPD",
                "DHP"
            ]
        }
```



**Third question** :  Only return a single possibility per delivery day per carrier

The file that solve this question is `getPossibilityDayPerCarrier.js`

Input is :  (`day`, `carrier`, `fromCountry`, `toCountry`)
please note that you should send this data as a query parameters, example:

```
https://AWS_URL?day=2021-05-30&carrier=PNL&fromCountry=nl&toCountry=fr
```

### How I solved it:

- I guess it is the same as the first question but for one day and for one carrier, and I used this function and called it inside the first question while iterating inside each carrier. 






